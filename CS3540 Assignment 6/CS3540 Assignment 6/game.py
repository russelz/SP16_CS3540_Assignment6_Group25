﻿from player import player

class game(object):
    """This class handles the logic for the game of bowling."""

    def __init__(self, numPlayers):
        self.numPlayers = numPlayers
        self.playerList = []
        self.currentPlayers = 0


    def addPlayer(self, playerName):
        if self.numPlayers > len(self.playerList):
            self.playerList.append(player(playerName, self.currentPlayers))
            self.currentPlayers = self.currentPlayers+1
        else:
            print()
            print("Error: Player cap reached for this game.")
            print()

    def addToScore(self, roll)
        if roll == "x" or "X":
            
