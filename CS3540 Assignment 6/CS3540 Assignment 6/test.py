﻿import unittest
from CS3540_Assignment_6 import *
from player import player
from game import game

class Test_test1(unittest.TestCase):
    def test_SetNameTest(self):
        testName = "Test"

        testPlayer = player(testName, 0)

        self.assertEqual(testName, testPlayer.name)

    def test_SetRankTest(self):
        testRank = 0

        testPlayer = player("Test", testRank)

        self.assertEqual(testRank, testPlayer.rank)

    def test_AddToScoreTest(self):
        testPlayer = player("Test", 0)

        testPlayer.addToScore(5)
        testPlayer.addToScore(7)

        self.assertEqual(testPlayer.score, 12)

    def test_addPlayerToGameTest(self):
        testGame = game(1)

        testGame.addPlayer("Test")

        player1 = player("Test", 0)

        self.assertEqual(testGame.playerList[0].name, player1.name)

    def test_maxPlayerCountTest(self):
        numPlayersInGame = 4
        
        testGame = game(numPlayersInGame)

        testGame.addPlayer("Test1")
        testGame.addPlayer("Test2")
        testGame.addPlayer("Test3")
        testGame.addPlayer("Test4")
        testGame.addPlayer("Test5")

        self.assertEqual(numPlayersInGame, testGame.currentPlayers)

    def test_pins(self):
        testPlayer = player("test", 0)

        testPlayer.addToScore(14)

        self.assertFalse(testPlayer.score, 14)

    def test_strike(self):
        testPlayer = player("test", 0)

        testPlayer.Special_roll("X")

        self.assertEqual(testPlayer.Special_roll, "X")

    def test_spare(self):
        testPlayer = player("test", 0)

        testPlayer.Special_roll("/")

        self.assertEqual(testPlayer.Special_roll, "/")

    def test_gutter(self):
        testPlayer = player("test", 0)

        testPlayer.Special_roll("-")

        self.assertEqual(testPlayer.Special_roll, "-")

    def test_gutter(self):
        testPlayer = player("test", 0)

        testPlayer.Special_roll("r")

        self.assertFalse(testPlayer.Special_roll, "r")    



if __name__ == '__main__':
    unittest.main()
    